# Modified tensorflow serving (py2, gpu)

## Requires
- cuda
- docker
- nvidia-docker

<br>
## How to build
```shell
$ docker build -t tf_serving .
```

<br>
## Run container
```shell
$ nvidia-docker run -it -d -P --name tf_serving -p 3000:3000 tf_serving
```

<br>
## Model file copy into container
```shell
$ docker cp /model/path tf_serving:/models
```

<br>
## Execute tensorflow_model_server
```shell
$ docker exec -it tf_serving /bin/bash
$ tensorflow_model_server \
	 --port=3000 \
	 --model_name=MODEL_NAME \
	 --model_base_path=/models &> /models/log &
```

<br>
---
