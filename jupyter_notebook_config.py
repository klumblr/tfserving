import os
from IPython.lib import passwd

c = c  # pylint:disable=undefined-variable
c.NotebookApp.ip = '0.0.0.0'
c.NotebookApp.port = int(os.getenv('PORT', 8888))
c.NotebookApp.open_browser = False

# password : admin
c.NotebookApp.password = 'sha1:cf897c5d5f87:feeb850fbb019f1c125e4cceff367b4dd104d736'
c.NotebookApp.notebook_dir = '/content'
